package com.mycompany.calculatorapp;

public class CalculatorFactory {

    private Calculator calculator;

    public static Calculator create(CalculatorType type) {
        switch (type) {
            case TRADITIONAL:
                return new TraditionalCalcu();
            case SCIENTIFIC:
                return new ScientificCalcu();
        }
        throw new IllegalArgumentException("There is no calculator like that!");
    }
}
