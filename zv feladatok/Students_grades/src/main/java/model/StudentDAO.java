
package model;

import entities.StudentEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.event.CaretEvent;

@Singleton
public class StudentDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public StudentEntity findStudentById(long id){
         return em.find(StudentEntity.class, id);
    }
    
    public List<StudentEntity> findAllStudents(){
        return em.createQuery("SELECT s FROM StudentEntity s", StudentEntity.class).getResultList();
    }
    
    public void addNewStudent(StudentEntity entity) {
        try {
            em.persist(entity);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
