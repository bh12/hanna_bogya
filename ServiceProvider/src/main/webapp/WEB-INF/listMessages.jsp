<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Messages</title>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>
        <c:if test="${listMessageError != null}">
            <div class="alert alert-danger" role="alert">
                ${listMessageError}
            </div>
        </c:if>
        <c:if test="${deleteMessageError != null}">
            <div class="alert alert-danger" role="alert">
                ${deleteMessageError}
            </div>
        </c:if>

        <h1>Messages</h1>
        <main role="main" class="container">
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <h3 class="border-bottom border-gray pb-2 mb-0">Incoming messages</h3>
                <c:choose>
                    <c:when test="${empty incomingMessageDTOs}">
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">No messages :(</strong>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${incomingMessageDTOs}" var="incomingMessage">
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">AT:  ${incomingMessage.timeOfSending}  FROM:  ${incomingMessage.senderName}  SUBJECT:  ${incomingMessage.subject}</strong>
                                    ${incomingMessage.message}
                                </p>
                                <p>
                                <form action="SendMessageServlet" method="GET">
                                    <button class="btn btn-outline-dark" id="messageid" name="messageid" value="${incomingMessage.id}">Reply</button>  
                                </form>
                                <form action="MessagesServlet" method="POST">
                                    <button class="btn btn-outline-dark" id="deleteIncomingMessageButton" name="deleteIncomingMessageButton" value="${incomingMessage.id}" type="submit">Delete</button>
                                </form>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>

            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <h3 class="border-bottom border-gray pb-2 mb-0">Sent messages</h3>
                <c:choose>
                    <c:when test="${empty sentMessageDTOs}">
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">No messages :(</strong>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${sentMessageDTOs}" var="sentMessage">
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">AT:  ${sentMessage.timeOfSending}  TO:  ${sentMessage.recipientName}  SUBJECT:  ${sentMessage.subject}</strong>
                                    ${sentMessage.message}
                                </p>

                                <p>
                                <form action="MessagesServlet" method="POST">
                                    <button class="btn btn-outline-dark" id="deleteSentMessageButton" name="deleteSentMessageButton" value="${sentMessage.id}">Delete</button>
                                </form>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>
        </main>
    </body>
</html>
