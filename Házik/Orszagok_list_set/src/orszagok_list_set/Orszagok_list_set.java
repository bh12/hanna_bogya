/*
 Legyenek Orszagaink, abban vannak Varosok. Varosoknak van nepesseguk es nevuk. 
 Akkor egyenlo ket orszag, ha ugyan azok a varosok vannak bennuk. Es akkor egyenlo
 ket varos, ha a nevuk es a nepesseguk megegyezik egymassal. Csinaljatok teszt 
 orszagokat, duplikaciokkal egyutt, es rakosgassatok oket be valamilyen Listbe. 
 Toroljetek a listabol egy adott orszagot. Valamint ellenorizzetek, hogy ugyan 
 olyan orszag nem kerul be duplikalva egy HashSetbe. Rakjatok bele ezeket orszagokat
 egy TreeSetbe is az orszag ossznepessege alapjan ami a varosok nepessegenek az osszege. 
 */
package orszagok_list_set;

import java.util.ArrayList;
import java.util.List;

public class Orszagok_list_set {

    public static void main(String[] args) {
        List<City> citiesOfIsrael = new ArrayList<>();
        citiesOfIsrael.add(new City("Jerusalem", 12));
        citiesOfIsrael.add(new City("TelAviv", 13));
        citiesOfIsrael.add(new City("BeerSheva", 9));
        citiesOfIsrael.add(new City("BneiBrak", 10));

        List<City> citiesOfRomania = new ArrayList<>();
        citiesOfRomania.add(new City("Bucuresti", 40));
        citiesOfRomania.add(new City("Sibiu", 25));
        citiesOfRomania.add(new City("Constanta", 19));

        List<City> citiesOfSweden = new ArrayList<>();
        citiesOfSweden.add(new City("Stockholm", 45));
        citiesOfSweden.add(new City("Göteborg", 40));
        citiesOfSweden.add(new City("Malmö", 30));

        Country romania = new Country("Romania", citiesOfRomania);
        Country israel = new Country("Israel", citiesOfIsrael);
        Country sweden = new Country("Sweden", citiesOfSweden);

        List<Country> countries = new ArrayList<>();
        countries.add(sweden);
        countries.add(israel);
        countries.add(romania);

        System.out.println(countries.size());
        countries.remove(romania);

        System.out.println(countries.size());
        
    }

}
