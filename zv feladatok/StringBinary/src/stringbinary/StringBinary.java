/*
Irjatok megoldast a kovetkezo problemara: Adott egy string mely reprezental 
binarisan egy szamot (pl “1011” = 11). Ezzel a szammal a kovetkezo muveletek fognak
vergehajtodni: Ha a szam paros akkor osszuk el 2-vel, ha paratlan akkor vonjunk ki
belole egyet. Add meg mennyi muvelettel erjuk el amig a szam 0 nem lesz.
 */
package stringbinary;

/**
 *
 * @author Johanna
 */
public class StringBinary {

    public static void main(String[] args) {

        String s = "1010010";
        int value = toInt(s);
        System.out.println("s" + s);
        int countOfOp = countOfOperations(value);
        System.out.println("count: " + countOfOp);
    }

    public static int toInt(String s) {
        int conv;
        try {
            conv = Integer.parseInt(s,2);
        } catch (NumberFormatException e) {
            conv = 0;
        }
        return conv;
    }

    public static int countOfOperations(int k) {
        int counter = 0;

        do {
            if (k % 2 == 0) {
                k = k / 2;
                counter++;
            } else {
                k -= 1;
                counter++;
            }            

        } while (k != 0);
        
        return counter;
    }

}
