package employeewriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class Writer {

   
    public void writeEmployeesInFile(List<Employee> employees) {

        File file = new File("employees.ser");

        try (FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
        
           for(Employee e : employees){
               oos.writeObject(e);           
           }

        } catch (IOException ex) {
            System.out.println("Problem while writing your employees");
        }

    }
}
