package model;

import entity.Car;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class CarDAO {

    @PersistenceContext
    private EntityManager em;


    public Car returnCarWithTheLessKm() {
        final Car min = em.createQuery("SELECT c FROM Car c WHERE c.km = (SELECT MIN(c.km) FROM Car c)", Car.class).getSingleResult();
        
        return min;
    }

    public List<Car> getCars() {
        List<Car> cars = em.createQuery("SELECT c FROM Car c").getResultList();
        return cars;
    }

    public Car findCarById(Long id) {
       return em.find(Car.class, id);
    }

    public void addNewCar(Car car) {
        try {
            em.persist(car);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
