
package animal_file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Animal_file {

    public static void main(String[] args) {
        File animals = new File("animals.csv");
       
        List<String[]> animalList = ReadCSVFile(animals);
        howManyAnimalsAndCharacteristicsAre(animalList);
        hairyAnimals(animalList);
        animalsWithFeather(animalList);
        nonBreathingAnimals(animalList);
        backbonedAndPredatorAnimals(animalList);
        sortingByClassType(animalList);
        sortingByNumberOfLegs(animalList);
    }

    public static List<String[]> ReadCSVFile(File animals) {
        List<String[]> animalList = new ArrayList<>();
        try (BufferedReader buffR = new BufferedReader(new FileReader(animals))) {
            String line = buffR.readLine();

            while ((line = buffR.readLine()) != null) {
                String[] tmp = line.split(";");
                animalList.add(tmp);
            }
        } catch (IOException ioe) {
            System.out.println("Hiba a beolvasásban!");
        }
        return animalList;
    }

    public static void howManyAnimalsAndCharacteristicsAre(List<String[]> animalList) {
        int counter = 0;
        for (String[] s : animalList) {
            if (!s[0].equalsIgnoreCase("")) {
                counter++;
            }
        }
        System.out.println("Összesen " + counter + " állatot tartalmaz a lista.");
        for (String[] s : animalList) {
            System.out.println("Összesen " + (s.length - 1) + " tulajdonságot tartalmaz a lista.");
            break;
            }
        }
        
    public static void hairyAnimals(List<String[]> animalList) {
        int countHairyAnimals = 0;
        int countBaldAnimals = 0;
        for (String[] s : animalList) {
            if (s[1].equalsIgnoreCase("1")) {
                countHairyAnimals++;
            } else {
                countBaldAnimals++;
            }
        }
        System.out.println("Szőrös állatok száma: " + countHairyAnimals);
        System.out.println("Szőr nélküli állatok száma: " + countBaldAnimals);
    }

    public static void animalsWithFeather(List<String[]> animalList) {
        int countFeather = 0;
        int countWithoutFeather = 0;
        for (String[] s : animalList) {
            if (s[2].equalsIgnoreCase("1")) {
                //System.out.println(Arrays.toString(s));
                countFeather++;
            } else {
                countWithoutFeather++;
            }
        }
        System.out.println("Tollas állatok száma: " + countFeather);
        System.out.println("Toll nélküli állatok száma: " + countWithoutFeather);
    }

    public static void nonBreathingAnimals(List<String[]> animalList) {
        System.out.println("Nem-lélegző állatok: ");
        for (String[] s : animalList) {
            if (s[10].equalsIgnoreCase("0")) {
                System.out.println(s[0].toString() + " ");
            }
        }
    }

    public static void backbonedAndPredatorAnimals(List<String[]> animalList) {
        System.out.println("Gerinces és ragadozó állatok tulajdonságaikkal: ");
        for (String[] s : animalList) {
            if (s[7].equalsIgnoreCase("1") && s[9].equalsIgnoreCase("1")) {
                System.out.println(Arrays.toString(s));
            }
        }
    }

    public static void sortingByClassType(List<String[]> animalList) {
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        int count5 = 0;
        int count6 = 0;
        int count7 = 0;

        for (String[] s : animalList) {
            switch (s[17]) {
                case "1":
                    count1++;
                    break;
                case "2":
                    count2++;
                    break;
                case "3":
                    count3++;
                    break;
                case "4":
                    count4++;
                    break;
                case "5":
                    count5++;
                    break;
                case "6":
                    count6++;
                    break;
                case "7":
                    count7++;
                    break;

            }
        }
        System.out.println("Type 1: " + count1);
        System.out.println("Type 2: " + count2);
        System.out.println("Type 3: " + count3);
        System.out.println("Type 4: " + count4);
        System.out.println("Type 5: " + count5);
        System.out.println("Type 6: " + count6);
        System.out.println("Type 7: " + count7);

    }

    public static void sortingByNumberOfLegs(List<String[]> animalList) {
        int count0 = 0;
        int count2 = 0;
        int count4 = 0;
        int count5 = 0;
        int count6 = 0;
        int count8 = 0;

        for (String[] s : animalList) {
            switch (s[13]) {
                case "0":
                    count0++;
                    break;
                case "2":
                    count2++;
                    break;
                case "5":
                    count5++;
                    break;
                case "4":
                    count4++;
                    break;
                case "6":
                    count6++;
                    break;
                case "8":
                    count8++;
                    break;

            }
        }
        System.out.println("Without legs: " + count0);
        System.out.println("Two legged: " + count2);
        System.out.println("Four legged: " + count4);
        System.out.println("Five legged: " + count5);
        System.out.println("Six legged: " + count6);
        System.out.println("Eight legged: " + count8);

    }

    public static void printArrayList(List<String> ArrayList) {
        for (String s : ArrayList) {
            System.out.println(s);
        }
    }

    public static void CreateArrayList(File f, int counter, List<String> ArrayList) {
        try (BufferedReader buffR = new BufferedReader(new FileReader(f))) {
            for (int i = 0; i < counter; i++) {
                String line = buffR.readLine();
                ArrayList.add(line);
            }

        } catch (IOException ioe) {
            System.out.println("Hiba a beolvasásban!");
        }
    }

    public static int lineCounter(File animals) {
        int lineCounter = 0;
        try (BufferedReader buffR = new BufferedReader(new FileReader(animals))) {
            String line = buffR.readLine();

            while (line != null) {
                line = buffR.readLine();
                lineCounter++;
            }
        } catch (IOException ioe) {
            System.out.println("Hiba a beolvasásban!");
        }
        return lineCounter;
    }

}
