package multithreadswritingme;

public class MitIrKi {

    public static void main(String[] args) {

//        float a = 4;
//        float b = 7;
//        float c = a + b;
//
//        int[] arr = {15, 2, 1, 23, 0, 8};
//        buborekRendezes(arr);
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println(arr[i]);
//        }
//        System.out.println(getMaxItem(arr));
       // recursion(12);
       String s1 = "alma";
       String s2 = "alma";
        System.out.println(s1 == s2);
        String s3 = new String(s1);
        System.out.println(s1.equals(s3));
        System.out.println(s2 == s3);

    }

    public static void recursion(int m) {
        if(m % 2 == 0){
            System.out.println(m);
            recursion(++m);
        } else recursion(--m);
    }

    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static int getMaxItem(int[] array) {
        int maxItem = array[0];

        for (int i = 0; i < array.length; i++) {
            if (maxItem < array[i]) {
                maxItem = array[i];
            }

        }
        return maxItem;
    }

    public static void buborekRendezes(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[i] > array[j]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }
}
