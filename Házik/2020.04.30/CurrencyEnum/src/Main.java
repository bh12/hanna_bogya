/*
 Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, USD, JPN, RMB) 
 és a forintbeli értékét integerként. Hozzunk létre egy Person osztályt is, amiben minden
 személynél eltároljuk, hogy melyik pénznemből hány darab van neki. Tároljuk el még a személy
 korát és nevét is.
 Generáljunk véletlenszerűen embereket különböző típusú és mennyiségű pénzzel. Írjuk ki a 
 konzolra a következőket:
 a.    az emberek vagyonának összértékét
 b.    az emberek vagyonának átlagát
 c.    az emberek vagyonának átlagát 18-49 korosztályban
 d.    az emberek vagyonának mediánját
 e.    az emberek nevét vagyonát növekvő sorrendben
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Person p1 = randomPersonGenerator();
        Person p2 = randomPersonGenerator();
        Person p3 = randomPersonGenerator();
        Person p4 = randomPersonGenerator();
        Person p5 = randomPersonGenerator();

        System.out.println("A teljes összeg forintban: " + sumOfAllTheMoney(p1, p2, p3, p4, p5));
        System.out.println("Az átlaga forintban: " + sumOfAllTheMoney(p1, p2, p3, p4, p5) / 5);

        List<Person> personsList = addPersonsToList(p1, p2, p3, p4, p5);
        sortByMoney(personsList);

        medianCalculator(personsList);
        averageOfPeopleBetweenAges(personsList, 18, 49);

    }

    public static void averageOfPeopleBetweenAges(List<Person> personsList, int min, int max) {
        int ageCount = 0;
        int ageSum = 0;
        for (Person p : personsList) {
            if (p.getAge() > min && p.getAge() < max) {
                ageCount++;
                ageSum += p.sumOfAllMoney();
            }
        }
         System.out.println(ageSum/ageCount);
    }

    public static void medianCalculator(List<Person> personsList) {
        int middle = personsList.size() / 2;
        middle = middle > 0 && middle % 2 == 0 ? middle : (middle + 1);

        System.out.println("Medián: " + personsList.get(middle).sumOfAllMoney());
    }

    public static void sortByMoney(List<Person> personsList) {
        Collections.sort(personsList);
        for (Person p : personsList) {
            System.out.println(p);
        }
    }

    public static List<Person> addPersonsToList(Person p1, Person p2, Person p3, Person p4, Person p5) {
        List<Person> personsList = new ArrayList<>();
        personsList.add(p1);
        personsList.add(p2);
        personsList.add(p3);
        personsList.add(p4);
        personsList.add(p5);

        return personsList;
    }

    public static int sumOfAllTheMoney(Person p1, Person p2, Person p3, Person p4, Person p5) {
        // az emberek összvagyonát
        int sum = p1.sumOfAllMoney() + p2.sumOfAllMoney()
                + p3.sumOfAllMoney() + p4.sumOfAllMoney() + p5.sumOfAllMoney();

        return sum;
    }

    public static Person randomPersonGenerator() {
        Person p1 = new Person();
        p1.setName(nameGenerator());
        p1.setAge((int) (Math.random() * 99) + 10);
        p1.setHowMuchEUR((int) (Math.random() * 100000));
        p1.setHowMuchGBP((int) (Math.random() * 10000));
        p1.setHowMuchHUF((int) (Math.random() * 1000000));
        p1.setHowMuchJPN((int) (Math.random() * 9000000));
        p1.setHowMuchRMB((int) (Math.random() * 800000));
        p1.setHowMuchUSD((int) (Math.random() * 100));
        return p1;
    }

    public static String nameGenerator() {
        Map<Integer, String> nameMap = new HashMap<>();
        nameMap.put(1, "John");
        nameMap.put(2, "Palpatine");
        nameMap.put(3, "Jackson");
        nameMap.put(4, "MaryAnn");
        nameMap.put(5, "Clara");
        nameMap.put(6, "Amelia");
        nameMap.put(7, "Joshua");
        nameMap.put(8, "Charlie");
        nameMap.put(9, "Eve");
        return nameMap.get((int) (Math.random() * 9) + 1);

    }

}
