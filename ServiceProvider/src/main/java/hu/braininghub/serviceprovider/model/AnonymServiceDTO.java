
package hu.braininghub.serviceprovider.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AnonymServiceDTO {
    
    private String name;
    private String image;
    private String shortDescription;
    
}
