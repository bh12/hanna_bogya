/*
írjunk servletet, mely POST paraméterben kapott szöveg fordítottját írja
ki a consolera, majd responseba megjeleníti hogy palindrom / nem palindrom
a kapott szöveg.
 */
package palindrom;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PalindromServlet", urlPatterns = {"/PalindromServlet"})
public class PalindromServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/palindrom.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final String word = request.getParameter("word");
        StringBuilder sb = new StringBuilder(word);
        String backwards = sb.reverse().toString();

        request.setAttribute("isPalindrome", backwards.equalsIgnoreCase(word));
        request.getRequestDispatcher("WEB-INF/palindrom.jsp").forward(request, response);

    }
}
