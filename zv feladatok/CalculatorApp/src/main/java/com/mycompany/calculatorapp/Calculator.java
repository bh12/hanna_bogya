package com.mycompany.calculatorapp;

public abstract class Calculator {
    
    public abstract void showYourUniqness();

    public double addition(double a, double b) {
        return a + b;
    }

    public double substraction(double a, double b) {
        return a - b;
    }

    public double multiplication(double a, double b) {
        return a * b;
    }

    public double division(int a, int b) throws ArithmeticException{
        return a / b;
    }

}
