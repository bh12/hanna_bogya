<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <script>
            $(document).ready(function () {
                var text_max = 1024;
                $('#character_feedback').html(text_max + ' characters remaining');

                $('#message').keyup(function () {
                    var text_length = $('#message').val().length;
                    var text_remaining = text_max - text_length;

                    $('#character_feedback').html(text_remaining + ' characters remaining');
                });
                $('#message').keyup();
            });
        </script>
        <title>Send message</title>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>
        <c:if test="${sendError != null}">
            <div class="alert alert-danger" role="alert">
                ${sendError}
            </div>
        </c:if>
        <h1>Send message</h1>
        <div class="container">
            <form method="POST" action="SendMessageServlet">
                <div class="form-group">
                    <label for="recipient">Recipient:</label>
                    <p>${recipentName}</p>
                </div>
                <div class="form-group">
                    <label for="subject">Subject: </label>
                    <input type="text" class="form-control" placeholder="Enter subject" id="subject" name="subject" maxlength="255">
                </div>
                <div class="form-group">
                    <label for="message">Message:</label>
                    <textarea class="form-control" placeholder="Enter message" id="message" name="message" rows="3" maxlength="1024"></textarea>
                    <div id="character_feedback"></div>  
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </body>
</html>
