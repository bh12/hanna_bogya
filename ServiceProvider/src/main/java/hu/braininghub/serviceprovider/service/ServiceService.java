package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.mapper.AnonymServiceMapper;
import hu.braininghub.serviceprovider.mapper.CategoryMapper;
import hu.braininghub.serviceprovider.mapper.ProviderMapper;
import hu.braininghub.serviceprovider.mapper.ServiceMapper;
import hu.braininghub.serviceprovider.model.AnonymServiceDTO;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.model.ServiceDAO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Stateless
public class ServiceService {

    @Inject
    ServiceDAO dao;

    public void addService(String name, String shortDescription, String description, String image, ProviderDTO providerDTO, List<CategoryDTO> categoryDTOs) {
        ServiceDTO dto = new ServiceDTO();
        dto.setName(name);
        dto.setShortDescription(shortDescription);
        dto.setDescription(description);
        dto.setImage(image);

        dto.setProviderDTO(providerDTO);

        dto.setCategoryDTOs(categoryDTOs);

        dao.addNewService(ServiceMapper.SERVICE_MAPPER.toEntity(dto));
    }

    public void updateService(ServiceDTO dto, String name, String shortDescription, String description, String pathToPersist, List<CategoryDTO> categoryDTOs, ProviderDTO providerDTO) {
        dto.setName(name);
        dto.setShortDescription(shortDescription);
        dto.setDescription(description);
        dto.setImage(pathToPersist);
        dto.setProviderDTO(providerDTO);
        dto.setCategoryDTOs(categoryDTOs);

        dao.updateService(ServiceMapper.SERVICE_MAPPER.toEntity(dto));

    }

    public void removeService(int serviceId) {
        dao.removeService(serviceId);
    }

    public ServiceDTO getServiceById(int id) {
        return ServiceMapper.SERVICE_MAPPER.toDTO(dao.getServiceById(id));
    }

    public List<ServiceDTO> getServices() throws SQLException {
        List<ServiceDTO> serviceDTOs = new ArrayList<>();

        for (ServiceEntity service : dao.getServices()) {
            serviceDTOs.add(ServiceMapper.SERVICE_MAPPER.toDTO(service));
        }

        return serviceDTOs;
    }

    public List<ServiceDTO> getServicesOfProvider(ProviderDTO providerDTO) throws SQLException {
        List<ServiceDTO> serviceDTOs = new ArrayList();

        for (ServiceEntity s : dao.getServicesOfProvider(ProviderMapper.PROVIDER_MAPPER.toEntity(providerDTO))) {
            serviceDTOs.add(ServiceMapper.SERVICE_MAPPER.toDTO(s));
        }

        return serviceDTOs;
    }

    public List<AnonymServiceDTO> getAnonymServices() throws SQLException {
        List<AnonymServiceDTO> anonymServiceDTOs = new ArrayList();

        for (ServiceEntity service : dao.getServices()) {
            anonymServiceDTOs.add(AnonymServiceMapper.INSTANCE.toAnonymDTO(service));
        }

        return anonymServiceDTOs;
    }

    public List<AnonymServiceDTO> getServicesAsAnonymUser() throws SQLException {

        return AnonymServiceMapper.INSTANCE.toAnonymDTOList(dao.getServices());
    }

    public List<AnonymServiceDTO> searchBoxForAnonymUsers(String searchedWord) {

        return AnonymServiceMapper.INSTANCE.toAnonymDTOList(dao.searchBoxForAnonymUsers(searchedWord));
    }

    public List<ServiceDTO> searchBox(String searchedWord) {

        List<ServiceEntity> filteredServiceList = dao.searchBox(searchedWord);
        List<ServiceDTO> filteredSList = new ArrayList<>();

        for (ServiceEntity s : filteredServiceList) {
            filteredSList.add(ServiceMapper.SERVICE_MAPPER.toDTO(s));
        }

        return filteredSList;
    }
}
