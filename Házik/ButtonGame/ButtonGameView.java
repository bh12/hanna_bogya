/*
 Csináljunk egy mini játékot. Hozzunk létre 9 darab gombot, használjuk a 
 Grid Layout-ot. Egy gomb megnyomására, tűnjön el egy random másik gomb, viszont 
 az eddig eltűntetett gomb jelenjen meg!
 */
package ButtonGame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonGameView extends JFrame implements ActionListener {
    
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
   
    
    private GridBagConstraints gbc = new GridBagConstraints();
    
    public ButtonGameView(String title) throws HeadlessException {
        super(title);
    }
    
    public void init() {
        basicSetup();
        buttonSetup();
        this.setVisible(true);
        
    }
    
    public void basicSetup() {
        this.setSize(400, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        
    }
    
    public void buttonSetup() {
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.insets = new Insets(7, 7, 7, 7);
        
        button1 = new JButton("1");
        button1.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(button1, gbc);
        
        button2 = new JButton("2");
        button2.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(button2, gbc);
        
        button3 = new JButton("3");
        button3.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 4;
        add(button3, gbc);
        
        button4 = new JButton("4");
        button4.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 6;
        add(button4, gbc);
        
        button5 = new JButton("5");
        button5.addActionListener(this);
        gbc.gridx = 2;
        gbc.gridy = 0;
        add(button5, gbc);
        
        button6 = new JButton("6");
        button6.addActionListener(this);
        gbc.gridx = 2;
        gbc.gridy = 2;
        add(button6, gbc);
        
        button7 = new JButton("7");
        button7.addActionListener(this);
        gbc.gridx = 2;
        gbc.gridy = 4;
        add(button7, gbc);
        
        button8 = new JButton("8");
        button8.addActionListener(this);
        gbc.gridx = 2;
        gbc.gridy = 6;
        add(button8, gbc);
        
        button9 = new JButton("9");
        button9.addActionListener(this);
        gbc.gridx = 4;
        gbc.gridy = 0;
        add(button9, gbc);
                
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Map<Integer, JButton> buttons = addingButtonsToMap();
        
        int rnd = (int) (Math.random() * 9) + 1;
        
        if (e.getSource() != buttons.get(rnd) && e.getSource() == button1
                || e.getSource() == button2 || e.getSource() == button3
                || e.getSource() == button4 || e.getSource() == button5
                || e.getSource() == button6 || e.getSource() == button7
                || e.getSource() == button8 || e.getSource() == button9) {
            
            for (Map.Entry<Integer, JButton> entry : buttons.entrySet()) {
                entry.getValue().setVisible(true);
            }            
            buttons.get(rnd).setVisible(false);
            
        }
        
    }

    public Map<Integer, JButton> addingButtonsToMap() {
        Map<Integer, JButton> buttons = new HashMap<>();
        buttons.put(1, button1);
        buttons.put(2, button2);
        buttons.put(3, button3);
        buttons.put(4, button4);
        buttons.put(5, button5);
        buttons.put(6, button6);
        buttons.put(7, button7);
        buttons.put(8, button8);
        buttons.put(9, button9);
        return buttons;
    }
    
}
