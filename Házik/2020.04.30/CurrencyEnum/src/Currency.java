
public enum Currency {
    EUR(363), 
    GBP(418), 
    HUF(1), 
    USD(334), 
    JPN(3), 
    RMB(46);
    
    private final int valueInHuf;

    public int getHuf() {
        return valueInHuf;
    }

    private Currency(int valueInHuf) {
        this.valueInHuf = valueInHuf;
    }
    
    public int valueInHuf(Currency me, Person p) {
        switch (me) {
            case EUR:
                return (int) (me.EUR.getHuf() * p.getHowMuchEUR());

            case GBP:
                return (int) (me.GBP.getHuf() * p.getHowMuchGBP());

            case HUF:
                return (int) (me.HUF.getHuf() * p.getHowMuchHUF());

            case JPN:
                return (int) (me.JPN.getHuf() * p.getHowMuchJPN());

            case RMB:
                return (int) (me.RMB.getHuf() * p.getHowMuchRMB());

            case USD:
                return (int) (me.USD.getHuf() * p.getHowMuchUSD());

            default:
                return -1;
        }

    }


}
