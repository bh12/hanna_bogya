package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.UserService;
import java.io.IOException;
import java.util.Enumeration;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {

    @Inject
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user == null) {
            request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/DemoServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Enumeration<String> parameterNames = request.getParameterNames();

        try {

            while (parameterNames.hasMoreElements()) {
                
                String nextElement = parameterNames.nextElement();
                
                if (nextElement == null) {
                    request.setAttribute("registerError", nextElement + " can not be NULL.");
                    request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                    break;
                }

                if (nextElement.isEmpty()) {
                    request.setAttribute("registerError", nextElement + " can not be EMPTY.");
                    request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                    break;
                }
            }
            
            request.setAttribute("emailAttempt", request.getParameter("email"));
            request.setAttribute("nameAttempt", request.getParameter("name"));

            if (!request.getParameter("email").equals(request.getParameter("confirm_email"))) {
                request.setAttribute("registerError", "Email address does not math.");
                request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                return;
            }

            if (!request.getParameter("password").equals(request.getParameter("confirm_password"))) {
                request.setAttribute("registerError", "Passwords does not math.");
                request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                return;
            }

            userService.adduser(
                    request.getParameter("email"),
                    request.getParameter("name"),
                    request.getParameter("password")
            );

        } catch (Exception ex) {
            request.setAttribute("registerError", ex.getMessage());
            request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
            return;
        }

        request.removeAttribute("registerError");
        request.setAttribute("loginAttempt", request.getParameter("email"));
        request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
    }

}
