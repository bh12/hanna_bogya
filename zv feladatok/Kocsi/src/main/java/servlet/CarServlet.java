package servlet;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.CarDTO;
import service.CarService;

@WebServlet(name = "CarServlet", urlPatterns = {"/CarServlet"})
public class CarServlet extends HttpServlet {

    @Inject
    CarService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/addNewCar.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String plateNumber = request.getParameter("plateNumber");
        String type = request.getParameter("type");
        String kilometers = request.getParameter("km");

        Long km;

        try {

            km = Long.parseLong(kilometers);

            service.addNewCar(plateNumber, type, km);
            List<CarDTO> cars = service.getCarDTOs();

            request.setAttribute("carDTOs", cars);
            request.setAttribute("bestCar", service.returnCarWithTheLessKm());           
            request.getRequestDispatcher("WEB-INF/success.jsp").forward(request, response);

        } catch (NumberFormatException ex) {
            request.setAttribute("kmError", "Please, enter valid km!");
            request.getRequestDispatcher("WEB-INF/addNewCar.jsp").forward(request, response);
        }

    }

}
