
import com.mycompany.calculatorapp.Calculator;
import com.mycompany.calculatorapp.ScientificCalcu;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    Calculator c;

    public CalculatorTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        c = new ScientificCalcu();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void add_sumOf3and5_returns8() {
        double result = c.addition(3, 5);

        assertEquals(8, result);
    }

    @Test
    public void divide_4by2_returns2() {
        double result = c.division(4, 2);

        assertEquals(2, result);
    }

    @Test()
    public void divide_4by0_throwsException() {

        assertThrows(ArithmeticException.class, () -> c.division(4, 0));
    }

}
