/*
Olvassunk egy állományt hallgatókról, mely az alábbi formában van: 
név;elso_zh_eredmeny;masodik_zh_eredmeny;zv_eredmeny
Írjuk ki egy másik fájlba az alábbi formában: 
név;eredmenyek_atlaga
A konvertáláshoz használjunk lambda kifejezést (map)
 */
package students_file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Students_file {

    public static void main(String[] args) {
        
        writeInFile(mapStringArrayToString(readFileAndParse("students.txt", ";")), "targetFile.txt");
    }

    public static List<String[]> readFileAndParse(String fileName, String regex) {
        List<String[]> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

            String line;
            while ((line = br.readLine()) != null) {
                String[] tmp = line.split(regex);
                lines.add(tmp);
            }

        } catch (IOException e) {
            System.out.println("Error while reading file!" + e.getMessage());
        }
        return lines;
    }
    public static List<String> mapStringArrayToString(List<String[]> lines) {

        List<String> toBeWritten = lines.stream()
                .map((String[] s) -> {
                    String name = s[0];
                    double avg = (Double.valueOf(s[1])
                            + Double.valueOf(s[2]) + Double.valueOf(s[3])) / 3;
                    return name + ";" + String.format("%.2f", avg);
                }).collect(Collectors.toList());
        return toBeWritten;
    }

    public static void writeInFile(List<String> listToBeWritten, String targetFileName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(targetFileName))) {
            for (String s : listToBeWritten) {
                bw.write(s + "\n");
            }

        } catch (IOException ioe) {
            System.out.println("Error while writing file: " + ioe.getMessage());
        }
    }

    public static void writeStudentAndAvg(String fileName) {
        List<String[]> lines = readFileAndParse("students.txt", ";");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            double sum = 0;
            for (int i = 0; i < lines.size(); i++) {
                String[] temp = lines.get(i);
                for (int j = 1; j < temp.length; j++) {
                    sum += Double.valueOf(temp[j]);
                }
                bw.write(temp[0] + ";" + String.format("%.2f", (sum / 3)) + "\n");
                sum = 0;
            }

        } catch (IOException ioe) {
            System.out.println("Error while writing file: " + ioe.getMessage());
        }

    }

    public static void writeAndMap(String fileName) {
        List<String[]> lines = readFileAndParse("students.txt", ";");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            double sum = 0;
            for (int i = 0; i < lines.size(); i++) {
                String[] temp = lines.get(i);
                List<String> values = new ArrayList<>();
                for (int j = 1; j < temp.length; j++) {
                    values.add(temp[j]);
                }
                final Stream<Double> mappedList = values.stream().map(s -> Double.valueOf(s));
                final Iterator<Double> iterator = mappedList.iterator();
                while (iterator.hasNext()) {
                    double d = iterator.next();
                    sum += d;
                }
                bw.write(temp[0] + ";" + String.format("%.2f", (sum / 3)) + "\n");
                sum = 0;
            }

        } catch (IOException ioe) {
            System.out.println("Error while writing file: " + ioe.getMessage());
        }
    }    
}
