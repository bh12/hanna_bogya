package secondlinecopy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SecondLineCopy {

    public static void main(String[] args) {

        SecondLineWriting("source.txt","target1.txt" );
        writeEverySecondLine("source.txt","target2.txt");
        writeEverySecondLineBackwards("source.txt","target3.txt");
    }
    
    public static void SecondLineWriting(String sourceFile, String targetFile){
    try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                BufferedWriter bw = new BufferedWriter(new FileWriter(targetFile))) {
            int counter = 0;
            String line = br.readLine();
            List<String> lines = new ArrayList<>();
            while (line != null) {
                lines.add(line + "\n");
                counter++;
                line = br.readLine();
            }

            for (int i = 1; i < counter; i += 2) {
                bw.write(lines.get(i));
            }

        } catch (IOException ioe) {
            System.out.println("Error " + ioe.getMessage());
        }
    }

    public static void writeEverySecondLine(String sourceFile, String targetFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                BufferedWriter bw = new BufferedWriter(new FileWriter(targetFile))) {
            int counter = 0;
            String line = br.readLine();

            while (line != null) {
                line = br.readLine();

                if (counter % 2 == 0) {
                    bw.write(line + "\n");
                }
                counter++;
            }

        } catch (IOException ioe) {
            System.out.println("Error " + ioe.getMessage());
        }
    }

    public static void writeEverySecondLineBackwards(String sourceFile, String targetFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                BufferedWriter bw = new BufferedWriter(new FileWriter(targetFile))) {
            int counter = 0;
            String line = br.readLine();

            while (line != null) {
                line = br.readLine();

                if (counter % 2 == 0) {
                    StringBuilder sb = new StringBuilder(line);
                    bw.write(sb.reverse().toString() + "\n");
                }
                counter++;
            }

        } catch (IOException ioe) {
            System.out.println("Error " + ioe.getMessage());
        }

    }

}
