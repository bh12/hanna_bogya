package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.mapper.ProviderMapper;
import hu.braininghub.serviceprovider.model.ProviderDAO;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Stateless
public class ProviderService {

    @Inject
    ProviderDAO dao;

    public ProviderDTO getProviderById(int id) {
        return ProviderMapper.PROVIDER_MAPPER.toDTO(dao.getProviderById(id));
    }

    public List<ProviderDTO> getProviders() {
        List<ProviderDTO> providerDTOs = new ArrayList();

        for (ProviderEntity provider : dao.getProviders()) {
            providerDTOs.add(ProviderMapper.PROVIDER_MAPPER.toDTO(provider));
        }

        return providerDTOs;
    }

    public ProviderDTO getProviderByEmail(String email) {
        return ProviderMapper.PROVIDER_MAPPER.toDTO(dao.getProviderByEmail(email));
    }

    public void addProvider(String email, String phone, String address) {
        ProviderDTO dto = new ProviderDTO();
        dto.setEmail(email);
        dto.setPhone(phone);
        dto.setAddress(address);
        
        dao.addNewProvider(ProviderMapper.PROVIDER_MAPPER.toEntity(dto));
    }
}
