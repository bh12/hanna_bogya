<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>New Car</title>
    </head>
    <body>
    <c:if test="${kmError != null}">
        <div class="alert alert-danger" role="alert">
            ${kmError}
        </div>
    </c:if>
    <div class="container">
        <form method="POST" action="CarServlet">
            <br>
            <div class="form-group">
                <label for="plateNumber">Plate number:</label>
                <input type="text" class="form-control" placeholder="Enter plate number" id="plateNumber" name="plateNumber" required>
            </div>
            <div class="form-group">
                <label for="type">Type:</label>
                <input type="text" class="form-control" placeholder="Enter type" id="type" name="type" required>
            </div> 
            <div class="form-group">
                <label for="km">Kilometers:</label>
                <input type="text" class="form-control" placeholder="Enter kilometers" id="km" name="km" required>
            </div> 
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</body>
</html>
