package threadscanandwrite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WritingThread extends Thread {

    @Override
    public synchronized void run() {
        File file = new File("resultFile.txt");
        String line = null;
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(file))) {
            while (!ThreadScanAndWrite.stringQ.isEmpty()) {
                
                line = ThreadScanAndWrite.stringQ.poll();
                bf.write(line + System.lineSeparator());
                
            }

        } catch (IOException ioe) {
            System.out.println("Hiba a fájl írásánál!");
        }

    }

}
