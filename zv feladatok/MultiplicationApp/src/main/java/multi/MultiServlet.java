package multi;

import exception.WrongFormatException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MultiServlet", urlPatterns = {"/MultiServlet"})
public class MultiServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final String firstNumber = request.getParameter("first");
        final String secondNumber = request.getParameter("second");

        if (firstNumber == null || secondNumber == null) {
            request.getRequestDispatcher("WEB-INF/multi.jsp").forward(request, response);
        } else {

            if (!(isNumber(firstNumber) && isNumber(secondNumber))) {
                throw new WrongFormatException();
            } else {

                int first = Integer.parseInt(firstNumber);
                int second = Integer.parseInt(secondNumber);

                request.setAttribute("result", first + second);
                request.getRequestDispatcher("WEB-INF/multi.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    public boolean isNumber(String number) {

        return number.matches("^[0-9]+$");
    }

}
