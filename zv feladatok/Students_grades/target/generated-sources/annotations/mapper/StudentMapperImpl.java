package mapper;

import entities.StudentEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import model.StudentDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-24T20:30:45+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_261 (Oracle Corporation)"
)
public class StudentMapperImpl implements StudentMapper {

    @Override
    public StudentDTO toDTO(StudentEntity entity) {
        if ( entity == null ) {
            return null;
        }

        StudentDTO studentDTO = new StudentDTO();

        studentDTO.setId( entity.getId() );
        studentDTO.setName( entity.getName() );
        studentDTO.setCourse( entity.getCourse() );
        studentDTO.setAverage( entity.getAverage() );

        return studentDTO;
    }

    @Override
    public List<StudentDTO> toDTOList(List<StudentEntity> entities) {
        if ( entities == null ) {
            return null;
        }

        List<StudentDTO> list = new ArrayList<StudentDTO>( entities.size() );
        for ( StudentEntity studentEntity : entities ) {
            list.add( toDTO( studentEntity ) );
        }

        return list;
    }

    @Override
    public StudentEntity toEntity(StudentDTO dto) {
        if ( dto == null ) {
            return null;
        }

        StudentEntity studentEntity = new StudentEntity();

        studentEntity.setId( dto.getId() );
        studentEntity.setName( dto.getName() );
        studentEntity.setCourse( dto.getCourse() );
        studentEntity.setAverage( dto.getAverage() );

        return studentEntity;
    }

    @Override
    public List<StudentEntity> toEntityList(List<StudentDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<StudentEntity> list = new ArrayList<StudentEntity>( dtos.size() );
        for ( StudentDTO studentDTO : dtos ) {
            list.add( toEntity( studentDTO ) );
        }

        return list;
    }
}
