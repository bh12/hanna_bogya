package com.mycompany.calculatorapp;

public enum CalculatorType {
    TRADITIONAL,
    SCIENTIFIC
}
