/*
 2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű 
 tömböt, majd csináljon statisztikát róla, melyik szám hányszor fordul elő!
 */
package hw_tomb_02;

public class Hw_tomb_02 {

    public static void main(String[] args) {

        int[] array = fillRandomArray(500, 10, 100);
        int[] counterIdx = new int[100];
        printArray(array);
        System.out.println("");
        counterItems(array, counterIdx);

    }

    public static int[] fillRandomArray(int size, int min, int max) {
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max - min) + min);
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
    }

    public static void counterItems(int[] array, int[] counterItems) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] <= 99) {
                counterItems[array[i]]++;
            }
            System.out.println(array[i] + ": " + counterItems[array[i]]);
        }
    }
}
