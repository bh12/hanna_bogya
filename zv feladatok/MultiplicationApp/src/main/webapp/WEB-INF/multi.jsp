
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Multiplication</title>
    </head>
    <body>
        <div class="container">           
            <form method="GET" action="MultiServlet">
                <div class="form-group">
                    <label for="first">First number:</label>
                    <input type="text" class="form-control" placeholder="Enter a number" name="first" id="first" required>
                </div>
                <br>
                <div class="form-group">
                    <label for="second">Second number:</label>
                    <input type="text" class="form-control" placeholder="Enter a number" name="second" id="second" required>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <br> 
        <div class="form-group">
            <label for="result">Result: ${result}</label>
        </div>

    </body>
</html>
