/*

 +Amennyiben adtak megfelelő parancssori paramétereket, a felhasználótól ne is
 kérjünk be számokat, hanem hasnáljuk azokat a számokat.
 */
package hw_matrix_sok;

import java.util.Scanner;

public class Hw_matrix_sok {

    public static void main(String[] args) {
        int size;
        int szorzo;
        
        for (String argString : args) {
        
        szorzo = Integer.valueOf(argString);
        size = Integer.valueOf(argString);
        
        
        if (size > 2 && size < 6) {
        size = Integer.valueOf(argString);
        } else {
    
        Scanner sc = new Scanner(System.in);
        
        do {
            System.out.println("Add meg a mátrix méretét: ");
            size = sc.nextInt();
        } while (size < 2 || size > 6); 
        }

        int[][] array = new int[size][size];
        int[][] sumArray = new int[size][size];
        int[][] sumArray2 = new int[size][size];
        
        printMultiArray(array);   // kiíratás alapértékekekkel;

        printSeparator();

        fillMultiArray(array, 5, 129);    // feltöltés [5, 129] random;

        printMultiArray(array);
        
        printSeparator();
        
        sumMultiArray(array, sumArray);
        printMultiArray(sumArray);
        
        printSeparator();
        initializeMatrixWithZero(array);
        fillMultiArrayWithNumber(array, 5);
        printMultiArray(array);
        
        printSeparator();
        makingProd(array, szorzo);
        printMultiArray(array);
        
        printSeparator();
        sumMultiArray(array, sumArray2);
        
        initializeMatrixWithZero(array);
        
}
    }

    public static void printMultiArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void fillMultiArray(int[][] array, int min, int max) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (Math.random() * (max - min + 1) + min);
            }
        }
    }
    
    public static void fillMultiArrayWithNumber(int[][] array, int number){
    
    for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = number;
            }
        }
    }
    
    public static void printSeparator() {
        System.out.println("--------------");
    }

    public static int[][] sumMultiArray(int[][] array1, int[][] sumArray) {
      for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
            sumArray[i][j] = array1[i][j] + array1[i][j];
        }
     }
        return sumArray;
    }
    
    public static void initializeMatrixWithZero(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = 0;
            }
        }
    }
    
    public static void makingProd(int[][] array, int szorzo){ 
    /*Scanner sc = new Scanner(System.in);
    
    do {
            System.out.println("Add meg a szorzó értékét: ");
            szorzo = sc.nextInt();
        } while (szorzo < 2 || szorzo > 6); */
        
        
    
     for (int i = 0; i < array.length; i++) {                // páros elemek
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] % 2 == 0) {
                    array[i][j] = szorzo * -1 * array[i][j];
                }
                
                if (array[i][j] % 2 == 1) {
                    array[i][j] = szorzo * array[i][j];
                
                }
            }
        }
    }
}
