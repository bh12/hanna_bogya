
package mapper;

import entities.StudentEntity;
import java.util.List;
import model.StudentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StudentMapper {
    
    StudentMapper STUDENT_MAPPER = Mappers.getMapper(StudentMapper.class);
    
    StudentDTO toDTO(StudentEntity entity);
    List<StudentDTO> toDTOList(List<StudentEntity> entities);
    
    StudentEntity toEntity(StudentDTO dto);
    List<StudentEntity> toEntityList(List<StudentDTO> dtos);
}
