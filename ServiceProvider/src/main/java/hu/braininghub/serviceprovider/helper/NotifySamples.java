package hu.braininghub.serviceprovider.helper;

public interface NotifySamples {
    String USER_REGISTER_SUBJECT = "Welcome to Service Provider by PKTP";
    String USER_REGISTER_TEXT = "Thank you for registering at Service Provider by PKTP!\n\nGo crazy!";
    String NEW_MESSAGE_SUBJECT = "You have a new message.";
    String NEW_MESSAGE_TEXT = "You can read the message at:\nhttp://localhost:8080/ServiceProvider/MessagesServlet\n";
    String PASSWORD_RECOVERY_SUBJECT = "There has been a password recovery request!";
}
