
package mapper;

import entity.Car;
import java.util.List;
import model.CarDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarMapper {
    CarMapper CAR_MAPPER = Mappers.getMapper(CarMapper.class); 
    
    CarDTO toDTO(Car car);
    List<CarDTO> toDTOList(List<Car> cars);
    
    Car toEntity(CarDTO dto);
    List<Car> toEntityList(List<CarDTO> dtos);   
    
}
