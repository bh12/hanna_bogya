package hu.braininghub.serviceprovider.service;

import java.util.Properties;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.LoggerFactory;

@Stateless
public class EmailService {

    private static final String FROM_EMAIL_ADDRESS = "team.pktp@gmail.com";
    private static final String SMTP_USERNAME = "team.pktp@gmail.com";
    private static final String SMTP_USER_PWD = "1D25C2551BAC161C3C8ACBE7E31C410D73E1";
    private static final String SMTP_HOST_ADDRESS = "smtp.elasticemail.com";
    private static final String SMTP_PORT = "2525";

    private Properties setSmtpProperties() {

        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", SMTP_HOST_ADDRESS);
        props.put("mail.smtp.port", SMTP_PORT);

        return props;
    }

    private Session getSession(Properties props) {
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SMTP_USERNAME, SMTP_USER_PWD);
            }
        });

        return session;
    }

    private Message createMessage(Session session, String recipientEmail, String emailSubject, String emailBody) throws MessagingException {

        Message message = new MimeMessage(session);
 
        message.setFrom(new InternetAddress(FROM_EMAIL_ADDRESS));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
        message.setSubject(emailSubject);
        message.setText(emailBody);
        
        return message;
    }

    private void sendMessage(Message message) throws MessagingException {

        Transport.send(message);
    }

    public void sendEmailNotification(String recipientEmail, String emailSubject, String emailBody) {
        try {
            sendMessage(createMessage(getSession(setSmtpProperties()), recipientEmail, emailSubject, emailBody));
        } catch (MessagingException ex) {
            LoggerFactory.getLogger(EmailService.class.getName()).error("Error by sending emial notification: {}", ex.getMessage());
        }
    }
}
