package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-09-24T20:30:35")
@StaticMetamodel(StudentEntity.class)
public class StudentEntity_ { 

    public static volatile SingularAttribute<StudentEntity, Double> average;
    public static volatile SingularAttribute<StudentEntity, String> name;
    public static volatile SingularAttribute<StudentEntity, String> course;
    public static volatile SingularAttribute<StudentEntity, Long> id;

}