/*
 Írj programot, ami listában tárolt Employee(név, kor, telefonszám, lakcím) 
típusú objektumokat kiír egy fájlba. 
 */
package employeewriter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeWriter {

    public static void main(String[] args) {

        Employee e1 = new Employee("First", 1, "5615", "Budapest");
        Employee e2 = new Employee("Second", 2, "56515", "Budapest");
        Employee e3 = new Employee("Third", 3, "56135", "Budapest");
        Employee e4 = new Employee("Fourth", 4, "56115", "Budapest");
        
        List<Employee> employees = new ArrayList<>();
        employees.add(e1);
        employees.add(e2);
        employees.add(e3);
        employees.add(e4);
        
        Writer employeeWriter = new Writer();
        employeeWriter.writeEmployeesInFile(employees);
    }
    
}
