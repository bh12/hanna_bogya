package servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.StudentService;

@WebServlet(name = "AddNewStudentServlet", urlPatterns = {"/AddNewStudentServlet"})
public class AddNewStudentServlet extends HttpServlet {

    @Inject
    StudentService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/addNewStudent.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String course = request.getParameter("course");
        String first = request.getParameter("first");
        String second = request.getParameter("second");
        String finalEx = request.getParameter("final");

        int firstEx;
        int secondEx;
        int finalExam;

        try {
            firstEx = Integer.parseInt(first);
            secondEx = Integer.parseInt(second);
            finalExam = Integer.parseInt(finalEx);

            if (!(!isValidGrade(firstEx) || !isValidGrade(secondEx) || !isValidGrade(finalExam))) {

                service.addNewStudent(name, course, firstEx, secondEx, finalExam);

                request.setAttribute("studentDTOs", service.findAllStudents());
                request.getRequestDispatcher("WEB-INF/success.jsp").forward(request, response);
                
            } else {
                request.setAttribute("gradeError", "Please, enter valid grade!");
                request.getRequestDispatcher("WEB-INF/addNewStudent.jsp").forward(request, response);
            }

        } catch (NumberFormatException ex) {
            request.setAttribute("gradeError", "Please, enter valid grade!");
            request.getRequestDispatcher("WEB-INF/addNewStudent.jsp").forward(request, response);
        }

    }

    public boolean isValidGrade(int n) {
        return n >= 1 && n <= 50;
    }
}
