//Írj többszálú alkalmazást, mely 5 szálon folyamatosan random számszor 
//kiírja a nevedet, számolva hogy hányszor írta ki.
package multithreadswritingme;

public class MultiThreadsWritingMe {

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            WriteMyNameThread wt = new WriteMyNameThread();
            wt.start();
        }
    }
}
