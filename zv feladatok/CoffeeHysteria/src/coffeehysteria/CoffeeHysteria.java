/*
Írjunk egy alkalamzást, mely véletlenszerűen generál egy booleant, mely ha hamis, 
kiírja hogy nincs kv, majd két szálon párhuzamosan elkezdi kiírni az alábbiakat tízszer: 
"csapkodom az ajtót" "kv-ért követelőzöm"
 */
package coffeehysteria;

import java.util.Random;

public class CoffeeHysteria {

    public static void main(String[] args) {
        Random rd = new Random();
        if(!rd.nextBoolean()){
            System.out.println("Nincs kávé");
            for(int i = 0; i < 2; i++){
                HystericalThread ht = new HystericalThread();
                ht.start();
            }
        }
        
    }

}
