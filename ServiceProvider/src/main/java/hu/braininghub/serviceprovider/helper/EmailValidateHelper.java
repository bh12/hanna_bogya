package hu.braininghub.serviceprovider.helper;

public class EmailValidateHelper {

    public static boolean isValidEmail(String email) {
        return email.matches("\\A[a-z0-9!#$%&'*+/=?^_‘{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_‘{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\z");
    }
}
