package whattime;

public class Clock {

    private int hour;
    private int minute;
    private int second;
    private int plusMinute;

    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour < 25) {
            this.hour = hour;
        } else {
            System.out.println("Error - hour");
        }
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute < 61) {
            this.minute = minute;
        } else {
            System.out.println("Error - minute");
        }
    }

    public void setSecond(int second) {
        if (second >= 0 && second < 61) {
            this.second = second;
        } else {
            System.out.println("Error - second");
        }
    }

    public String toString() {
        return "A pontos idő: " + hour + " óra " + minute + " perc és " + second + " másodperc";

    }

    public int getPlusMinute() {
        return plusMinute;
    }

    public void setPlusMinute(int plusMinute) {
        if ((minute + plusMinute) <= 60) {

            this.plusMinute = minute + plusMinute;
        } else if ((minute + plusMinute) > 60) {
            this.plusMinute = minute + plusMinute - 60;
            this.hour = hour + 1;
        }
    }

    public void printTime() {
        System.out.println("Az idő: " + getHour() + " : " + getMinute() + " : " + getSecond());
    }

    public void printModTime() {
        System.out.println("Az idő: " + getHour() + " óra " + getPlusMinute() + " perc " + getSecond() + " másodperc");
    }

}
