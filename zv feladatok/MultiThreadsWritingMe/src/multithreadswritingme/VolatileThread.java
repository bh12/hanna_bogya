
package multithreadswritingme;

import java.util.logging.Level;
import java.util.logging.Logger;

public class VolatileThread extends Thread {
    private static volatile int counter = 0;
    
    static void incrementNumber(String name) {
        System.out.println(Thread.currentThread().getName() + ": " + counter++ + name);
    }

    @Override
    public void run() {

        int rnd = (int) ((Math.random() * 300) + 5);
        while (counter <= rnd) {
            incrementNumber(" Hanna Volatile");
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(WriteMyNameThread.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }
    
}
