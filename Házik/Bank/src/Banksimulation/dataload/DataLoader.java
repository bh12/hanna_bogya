
package Banksimulation.dataload;


public interface DataLoader {
    
    public void loadInitialData();
    
}
