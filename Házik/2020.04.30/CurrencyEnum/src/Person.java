

public class Person implements Comparable<Person>{   
    
   private Currency moneyExchange;
   private int howMuchEUR;
   private int howMuchGBP;
   private int howMuchHUF;
   private int howMuchUSD;
   private int howMuchJPN;
   private int howMuchRMB;
   private int age;
   private String name;

    public Person() {
    }
    public Person(Currency moneyExchange, int howMuchEUR, int howMuchGBP, int howMuchHUF, int howMuchUSD, int howMuchJPB, int howMuchRMB, int age, String name) {
        this.moneyExchange = moneyExchange;
        this.howMuchEUR = howMuchEUR;
        this.howMuchGBP = howMuchGBP;
        this.howMuchHUF = howMuchHUF;
        this.howMuchUSD = howMuchUSD;
        this.howMuchJPN = howMuchJPB;
        this.howMuchRMB = howMuchRMB;
        this.age = age;
        this.name = name;
    }

    public Currency getMoneyExchange() {
        return moneyExchange;
    }

    public int getHowMuchEUR() {
        return howMuchEUR;
    }

    public void setHowMuchEUR(int howMuchEUR) {
        this.howMuchEUR = howMuchEUR;
    }

    public int getHowMuchGBP() {
        return howMuchGBP;
    }

    public void setHowMuchGBP(int howMuchGBP) {
        this.howMuchGBP = howMuchGBP;
    }

    public int getHowMuchHUF() {
        return howMuchHUF;
    }

    public void setHowMuchHUF(int howMuchHUF) {
        this.howMuchHUF = howMuchHUF;
    }

    public int getHowMuchUSD() {
        return howMuchUSD;
    }

    public void setHowMuchUSD(int howMuchUSD) {
        this.howMuchUSD = howMuchUSD;
    }

    public int getHowMuchJPN() {
        return howMuchJPN;
    }

    public void setHowMuchJPN(int howMuchJPN) {
        this.howMuchJPN = howMuchJPN;
    }

    public int getHowMuchRMB() {
        return howMuchRMB;
    }

    public void setHowMuchRMB(int howMuchRMB) {
        this.howMuchRMB = howMuchRMB;
    }

    public void setMoneyExchange(Currency moneyExchange) {
        this.moneyExchange = moneyExchange;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" + ", howMuchEUR=" + howMuchEUR +", sumOfMoney=" + sumOfAllMoney() + ", howMuchGBP=" + howMuchGBP + ", howMuchHUF=" + howMuchHUF + ", howMuchUSD=" + howMuchUSD + ", howMuchJPN=" + howMuchJPN + ", howMuchRMB=" + howMuchRMB + ", age=" + age + ", name=" + name + '}';
    }
    
    public int sumOfAllMoney() {
       return (Currency.EUR.getHuf() * getHowMuchEUR()) + (Currency.GBP.getHuf() * getHowMuchGBP()) + 
               (Currency.HUF.getHuf() * getHowMuchHUF()) + (Currency.JPN.getHuf() * getHowMuchJPN());

    }

    @Override
    public int compareTo(Person o) {
        if (this.sumOfAllMoney() < o.sumOfAllMoney()) return -1;
        else if(this.sumOfAllMoney() == o.sumOfAllMoney()) return 0;
        else return 1;
    }
   
    
    
}
