<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>All students</title>
    </head>
    <body>
        <h5>Student successfully added</h5>

       <div class="container">
            <table class="table table-dark table-sm">
                <thead>
                    <tr>
                        <th> ID</th>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Final grade</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${studentDTOs}" var="student">
                        <tr>
                            <td>${student.id}</td>
                            <td>${student.name}</td>
                            <td>${student.course}</td>
                            <td>${student.average}</td>  
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
