<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <%@include file="cssForHeadLine.jsp" %>
        <%@include file="head.jsp" %>
        <title>Login</title>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>

        <div class="container">
            <h1>Login:</h1>
            <form method="POST" action="LoginServlet" class="was-validated">
                <div class="form-group col-6">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" value="${loginAttempt}" id="email" name="email" required>
                </div>
                <div class="form-group col-6">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group col-6">
                    <c:if test="${loginError != null}">
                        <p class="alert alert-danger" role="alert">${loginError}</p>
                        <c:if test = "${loginError == 'Please enter validation code!'}">
                            <label for="validationCode">Validation code</label>
                            <input type="text" class="form-control" id="validationCode" name="validationCode">
                        </c:if>
                        <c:if test = "${loginError == 'Not registered user.'}">
                            <a href="RegisterServlet"><c:out value = "Would you like to register?" /></a>
                        </c:if>
                        <c:if test = "${loginError == 'Password does not match for registered email.'}">
                            <a href="PasswordRecoveryServlet?loginAttempt=${loginAttempt}"><c:out value = "Would you like to recover your password?" /></a>
                        </c:if>
                    </c:if>
                </div>
                <button type="submit" class="btn btn-outline-dark">Login</button>
            </form>
        </div>
        <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>
    </body>
</html>
