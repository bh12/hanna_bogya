package orszagok_list_set;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Country implements Comparable<Country>{
    private String nameOfCountry;
    private List<City> cities;
    private Set<City> setOfCities;

    public Country(String nameOfCountry, List<City> cities) {
        this.cities = cities;
        this.nameOfCountry = nameOfCountry;
        
    }
 
    public void setCities1(Set<City> cities1) {
        this.setOfCities = cities1;
    }

    public Country(String nameOfCountry, Set<City> cities1) {
        this.setOfCities = cities1;
        this.nameOfCountry = nameOfCountry;

    }

    public String getNameOfCountry() {
        return nameOfCountry;
    }

    public void setNameOfCountry(String nameOfCountry) {
        this.nameOfCountry = nameOfCountry;
    }

    public int sumPopulation(Country c) {
        c = new Country(this.getNameOfCountry(), setOfCities);
        int sumOfPopulation = 0;
        for (City i : setOfCities) {
            sumOfPopulation += i.getPopulation();

        }
        return sumOfPopulation;
    }

    @Override
    public int hashCode() {
        
        return this.setOfCities.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (!Objects.equals(this.cities, other.cities)) {
            return false;
        }
        if (!Objects.equals(this.setOfCities, other.setOfCities)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Country{" + "nameOfCountry=" + nameOfCountry +  ", listOfCities=" + setOfCities + '}';
    }

    

    @Override
    public int compareTo(Country o) {
        if (this.sumPopulation(this) < o.sumPopulation(o)) return -1;
        if (this.sumPopulation(this) == o.sumPopulation(o)) return 0;
        else return 1;
    
    
    }

    
    

    

}
