
package fruit_f;


public class Fruits {
    private double weight;   // kg
    private String colour;

    public Fruits(double weight, String colour) {
        this.weight = weight;
        this.colour = colour;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return "Fruits{" + "price=" + weight + ", colour=" + colour + '}';
    }
    
    
    
}
