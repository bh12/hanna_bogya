package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-09-20T15:31:45")
@StaticMetamodel(Car.class)
public class Car_ { 

    public static volatile SingularAttribute<Car, Long> km;
    public static volatile SingularAttribute<Car, Long> id;
    public static volatile SingularAttribute<Car, String> plateNumber;
    public static volatile SingularAttribute<Car, String> type;

}