/*
 Írjuk ki egy fájl tulajdonságait (fájl-e?, útvonal, abszolút útvonal, 
szülőmappa, fájl neve, mérete, olvasható-e, írható-e, rejtett-e, utolsó módosítás dátuma)
 */
package file_gyakorlatok;

import java.io.File;
import java.util.Date;

public class File_gyakorlatok {

    public static void main(String[] args) {
        File file = new File("C:\\Users\\Johanna\\Desktop\\BrainingHub\\1\\hanna_bogya\\TestTextFile");
        System.out.println("Abszolút file: " + file.getAbsoluteFile());
        System.out.println("Útvonal: " + file.getPath());
        System.out.println("Szülőmappa: " + file.getParent());
        System.out.println("Fájl neve: " + file.getName());
        System.out.println("Fájl rejtette-e: " + file.isHidden());
        System.out.println("Fájl-e: " + file.isFile());
        
        System.out.println("Dátum: " + file.lastModified());  // milliszkundumos 1970-től
        
        Date date = new Date(file.lastModified());        
        System.out.println("Utolsó módosítás ideje: " + date);
        
        System.out.println("Olvasható-e: " + file.canRead());
        System.out.println("Írható-e: " + file.canWrite());
    }
    
}
