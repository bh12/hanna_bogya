/*
Írjuk ki a felhasználó által megadott útvonalban lévő összes fájl illetve mappa nevet.

 */
package file_gyakorlatok;

import java.io.File;
import java.util.Scanner;

public class Second {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg az elérési útvonalat: ");
        String path = sc.nextLine();
        
        File file = new File(path);
        
        for(String s : file.list()) {
         System.out.println("A megadott útvonal összes mappája és fájlja: " + s);
        }
        
    }
}
