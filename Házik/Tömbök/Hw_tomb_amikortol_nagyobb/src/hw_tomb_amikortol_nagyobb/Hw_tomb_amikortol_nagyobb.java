/*
 2. generáljunk egy tömböt véletlen számokkal, majd írjuk ki azt a számot, 
amikortól kezdve tőle csak nagyobb szám van a tömbben. 
pl. 
3, 4, 7, 3, 4, 6 esetén a 3 a megoldás
9, 3, 1, 5, 2, 4, 6 esetén a 2 a megoldás
 */
package hw_tomb_amikortol_nagyobb;

public class Hw_tomb_amikortol_nagyobb {

    public static void main(String[] args) {
        
        int[] array = fillRandomArray(10, 1, 100);
        printArray(array);
        ascendingFromHere(array);
        
    }
    public static int[] fillRandomArray(int size, int min, int max) {
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max + 1 - min) + min);
        }
        return array;
    }
    
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println("");

    }
    
    public static void ascendingFromHere(int[] array) {
        int smallIndex = 0;   // deklarálok egy legkisebb indexet (ezt módosítom később)
        
        for (int i = 0; i < array.length; i++) {
            int smaller = array[smallIndex];   // az első elemet kinevezem legkisebbnek
            if (array[i] <= smaller){   // ha egy további elem kisebb, az indexe vándoroljon a smallerhez
            smallIndex = i;
            }
        }
        System.out.println("From here ascending: " + array[smallIndex]);
    }
}

