package hu.braininghub.serviceprovider.dao;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.model.ServiceDAO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ServiceDAOTest {
    
    @Mock
    EntityManager em;
    
    ServiceDAO serviceDAO;
    
    public ServiceDAOTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        em = Mockito.mock(EntityManager.class);
        serviceDAO = new ServiceDAO(em);
 
    }
    
    @AfterEach
    public void tearDown() {
    }

    
    @Test
    public void addNewService() {
        
        ServiceEntity s1 = new ServiceEntity();
        s1.setId(7);
        s1.setName("service1");
        s1.setShortDescription("short description1");
        s1.setDescription("longer description1");
        s1.setImage("images/cica.jpg");
        
        ProviderEntity p1 = new ProviderEntity();
        p1.setId(7);
        p1.setAddress("address1");
        p1.setEmail("provider1@email.hu");
        p1.setPhone("1111111");
        s1.setProvider(p1);
        
        CategoryEntity c1 = new CategoryEntity();
        c1.setId(7);
        c1.setName("category1");
        CategoryEntity c2 = new CategoryEntity();
        c2.setId(8);
        c2.setName("category2");
        CategoryEntity c3 = new CategoryEntity();
        c3.setId(9);
        c3.setName("category3");
        
        List<CategoryEntity> categories1 = new ArrayList<>();
        categories1.add(c1);
        categories1.add(c2);
        categories1.add(c3);
        
        s1.setCategories(categories1);
        
        Assert.assertNotNull(em);
        
        serviceDAO.addNewService(s1);
        
        verify(em).persist(s1);
        
    }
    
    @Test
    public void removeService() {
        
        ServiceEntity s1 = new ServiceEntity();
        s1.setId(7);
        s1.setName("service1");
        s1.setShortDescription("short description1");
        s1.setDescription("longer description1");
        s1.setImage("images/cica.jpg");
        
        ProviderEntity p1 = new ProviderEntity();
        p1.setId(7);
        p1.setAddress("address1");
        p1.setEmail("provider1@email.hu");
        p1.setPhone("1111111");
        s1.setProvider(p1);
        
        CategoryEntity c1 = new CategoryEntity();
        c1.setId(7);
        c1.setName("category1");
        CategoryEntity c2 = new CategoryEntity();
        c2.setId(8);
        c2.setName("category2");
        CategoryEntity c3 = new CategoryEntity();
        c3.setId(9);
        c3.setName("category3");
        
        List<CategoryEntity> categories1 = new ArrayList<>();
        categories1.add(c1);
        categories1.add(c2);
        categories1.add(c3);

        s1.setCategories(categories1);
        
        Assert.assertNotNull(em);
        
        when(em.find(ServiceEntity.class, 7)).thenReturn(s1);
        
        ServiceEntity s = serviceDAO.getServiceById(7);
        Assert.assertNotNull(s);
        
        serviceDAO.removeService(7);
        verify(em).remove(s);
        
    }
    
}
