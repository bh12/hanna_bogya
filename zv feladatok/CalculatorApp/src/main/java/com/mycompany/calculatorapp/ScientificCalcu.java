package com.mycompany.calculatorapp;

import java.util.Scanner;

public class ScientificCalcu extends Calculator {

    Scanner sc = new Scanner(System.in);
    CalculatorType type;
    
    
    @Override
    public double division(int a, int b) {
        return super.division(a, b);
    }

    @Override
    public double multiplication(double a, double b) {
        return super.multiplication(a, b);
    }

    @Override
    public double substraction(double a, double b) {
        return super.substraction(a, b);
    }

    @Override
    public double addition(double a, double b) {
        return super.addition(a, b);
    }

    @Override
    public void showYourUniqness() {
        System.out.println("I can show you the binary value of a number, try me, enter a number: ");
        int n = sc.nextInt();
         System.out.println(Integer.toBinaryString(n));
    }

}
