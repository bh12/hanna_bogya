package mapper;

import entity.Car;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import model.CarDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-20T15:31:53+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_261 (Oracle Corporation)"
)
public class CarMapperImpl implements CarMapper {

    @Override
    public CarDTO toDTO(Car car) {
        if ( car == null ) {
            return null;
        }

        CarDTO carDTO = new CarDTO();

        carDTO.setId( car.getId() );
        carDTO.setPlateNumber( car.getPlateNumber() );
        carDTO.setType( car.getType() );
        carDTO.setKm( car.getKm() );

        return carDTO;
    }

    @Override
    public List<CarDTO> toDTOList(List<Car> cars) {
        if ( cars == null ) {
            return null;
        }

        List<CarDTO> list = new ArrayList<CarDTO>( cars.size() );
        for ( Car car : cars ) {
            list.add( toDTO( car ) );
        }

        return list;
    }

    @Override
    public Car toEntity(CarDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Car car = new Car();

        car.setId( dto.getId() );
        car.setPlateNumber( dto.getPlateNumber() );
        car.setType( dto.getType() );
        car.setKm( dto.getKm() );

        return car;
    }

    @Override
    public List<Car> toEntityList(List<CarDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Car> list = new ArrayList<Car>( dtos.size() );
        for ( CarDTO carDTO : dtos ) {
            list.add( toEntity( carDTO ) );
        }

        return list;
    }
}
