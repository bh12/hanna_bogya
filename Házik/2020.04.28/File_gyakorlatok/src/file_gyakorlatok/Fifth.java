/*
 A program kérjen be egy elérési és egy cél útvonalat. Az elérési útvonalon lévő 
 fájlt tartalmával együtt másolja át a cél útvonalra. Ha a megadott fájl egy mappa,
 akkor dobjunk kivételt amit le is kezelünk. Ha bármi hiba történik (például a cél 
 útvonal ugyanaz mint az elérési útvonal) akkor is dobjunk kivételt, és kezeljük le.
 */
package file_gyakorlatok;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Fifth {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Kérlek add meg a másolandó útvonalat: ");
        String parent = sc.nextLine();
        System.out.println("Kérlek, add meg a cél útvonalat: ");
        String destination = sc.nextLine();

        File parentFile = new File(parent);
        File destinationFile = new File(destination);

        try (InputStream parentStream = new BufferedInputStream(new FileInputStream(parentFile));
                OutputStream destinationstream = new BufferedOutputStream(new FileOutputStream(destinationFile))) {

            BufferedReader buffR = new BufferedReader(new FileReader(parentFile));
            FileWriter fileW = new FileWriter(destination);
            String line = buffR.readLine();
            while (!line.equalsIgnoreCase("")) {

                line = buffR.readLine();
            }

        } catch (IOException ioe) {
            System.out.println("Baszki!");
        }

    }
}
