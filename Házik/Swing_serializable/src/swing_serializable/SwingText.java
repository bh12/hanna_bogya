package swing_serializable;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class SwingText extends JFrame implements ActionListener {

    private JTextArea text;
    private JButton readTextButton;
    private JButton saveButton;
   // private JButton serializingButton;
    private JFileChooser choose;

    public SwingText() {

    }

    public void init() {

        this.setSize(700, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridBagLayout());

        buttonSetup();
        textAreaSetup();

        this.setVisible(true);
    }

    public void buttonSetup() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(7, 7, 7, 7);

        readTextButton = new JButton("Read");
        readTextButton.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 3;
        gbc.gridwidth = 3;
        add(readTextButton, gbc);

        saveButton = new JButton("Save");
        saveButton.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridheight = 3;
        gbc.gridwidth = 3;
        add(saveButton, gbc);

        /* serializingButton = new JButton("Serialize");
         serializingButton.addActionListener(this);
         gbc.gridx = 0;
         gbc.gridy = 10;
         gbc.gridheight = 3;
         gbc.gridwidth = 3;
         add(serializingButton, gbc); */
        choose = new JFileChooser("C:\\Users\\Johanna\\Desktop\\BrainingHub\\OOP feladatok\\Swing_serilaizable");
        choose.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 15;
        gbc.gridheight = 3;
        gbc.gridwidth = 3;
        add(choose, gbc);
        choose.setVisible(false);
    }

    public void textAreaSetup() {
        GridBagConstraints gbc = new GridBagConstraints();
        text = new JTextArea();
        gbc.gridx = 10;
        gbc.gridy = 1;

        add(text, gbc);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == readTextButton) {
            choose.setVisible(true);
            File file = choose.getSelectedFile();

            if (file.getName().equals("dumbledore.txt")) {
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    String line = br.readLine();
                    StringBuilder sb = new StringBuilder(line);

                    while (line != null) {
                        line = br.readLine();
                        sb.append(line);
                    }
                    text.setText(sb.toString());

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(SwingText.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ioe) {
                    System.out.println("Hiba!");
                }
            } else if (file.getName().equals("yoda.ser")) {
                try (FileInputStream fis = new FileInputStream("yoda.ser");
                        ObjectInputStream ois = new ObjectInputStream(fis)) {
                    Book book = (Book) ois.readObject();
                    text.setText(book.toString());

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(SwingText.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(SwingText.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        } else if (e.getSource() == saveButton) {

            try (FileOutputStream fos = new FileOutputStream("yoda.ser");
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {

                Book yoda = new Book();
                yoda.setComp("In a dark place we find ourselves, and a little more knowledge lights our way.");
                oos.writeObject(yoda);

            } catch (IOException ex) {
                Logger.getLogger(Swing_serializable.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
