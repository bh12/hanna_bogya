package service;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import mapper.StudentMapper;
import model.StudentDAO;
import model.StudentDTO;

@Stateless
public class StudentService {

    @Inject
    StudentDAO dao;

    public void addNewStudent(String name, String course, int firstExam, int secondExam, int finalExam) {

        StudentDTO dto = new StudentDTO();
        dto.setName(name);
        dto.setCourse(course);
        dto.setAverage(findAvg(firstExam, secondExam, finalExam));

        dao.addNewStudent(StudentMapper.STUDENT_MAPPER.toEntity(dto));
    }

    public StudentDTO findStudentById(long id) {
        return StudentMapper.STUDENT_MAPPER.toDTO(dao.findStudentById(id));
    }

    public List<StudentDTO> findAllStudents() {
        return StudentMapper.STUDENT_MAPPER.toDTOList(dao.findAllStudents());
    }

    private double findAvg(int firstExam, int secondExam, int finalExam) {
        double avg = (firstExam + secondExam + finalExam) / 3.0;
        return Math.round(avg * 100.0) / 100.0;
    }
}
