/*
  3.    Parancssorból kapott Stringeket vizsgáljuk meg, és írjuk ki azt, hogy 
 „Malacod van!” ha van a paraméterek között „pig” érték.
 */
package hw_commlinearg_feladatok_2;

public class Hw_CommLineArg_feladatok_2 {

    public static void main(String[] args) {
       /* String animal1 = args[0];
        String animal2 = args[1];
        String animal3 = args[2];
        String animal4 = args[3];
        String animal5 = args[4]; */
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].contains("pig")) {
                System.out.println("Malacod van!");
            } else {
                System.out.println("Másod van :)");
                break;
            }
        }
        
    }
    
}
