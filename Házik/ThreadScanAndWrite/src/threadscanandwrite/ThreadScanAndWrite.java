package threadscanandwrite;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class ThreadScanAndWrite{

    public static final Queue<String> stringQ = new PriorityQueue<>();
    public static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Kérlek, add meg a sorokat (üres sor megadása - finish): ");
        
        String line = sc.nextLine();
        while (!line.equals("")) {
            line = sc.nextLine();
            stringQ.add(line);
            
        }
    
        WritingThread wt = new WritingThread();
        wt.start();
       
    }

}
