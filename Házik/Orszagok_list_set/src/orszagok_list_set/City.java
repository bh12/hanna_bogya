package orszagok_list_set;

import java.util.Objects;

public class City implements Comparable<City> {

    private String name;
    private int population;

    public City(String name, int population) {
        this.name = name;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public int hashCode() {
        return this.getPopulation();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final City other = (City) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.population != other.population) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "City{" + "name=" + name + ", population=" + population + '}';
    }

    @Override
    public int compareTo(City o) {
        if (this.getPopulation() < o.getPopulation()) {
            return -1;
        } else if (this.getPopulation() == o.getPopulation()) {
            return 0;
        } else {
            return 1;
        }

    }

}
