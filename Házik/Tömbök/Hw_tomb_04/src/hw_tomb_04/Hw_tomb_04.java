/*
 A program legyen képes ötös és hatos lottó egy lehetséges húzásának eredményét
 visszaadni. A program köszöntse a felhasználót, majd egy menüvel döntse el, hogy
 ötös vagy hatos lottó számokat adjon vissza (menü) 
 */
package hw_tomb_04;

import java.util.Scanner;

public class Hw_tomb_04 {

    public static final int EXIT_MENU_NUMBER = 3;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sayHello();
        int number;
        do {
            System.out.println("");
            System.out.println("Kérlek, adjmeg egy számot: ");
            System.out.println("1. heti ötös ");
            System.out.println("2. heti hatos ");
            System.out.println(EXIT_MENU_NUMBER + ". lépj ki ");

            number = sc.nextInt();
            switch (number) {
                case 1: otosLottoSzamok();
                    break;
                case 2: hatosLottoSzamok();
                    break;
            }
        } while (number != EXIT_MENU_NUMBER);
    }

    public static void sayHello() {
        System.out.println("Üdv a játékban!");
    }
    
    public static int randomGenerator(int min, int max) {
    int randomNumber = (int)(Math.random() * (max + 1 - min) + min);
    return randomNumber;
    }
    
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
    }

    public static void otosLottoSzamok() {
        int[] lottoSzamok = new int[5];
        boolean nemMegfelelo;

        for (int i = 0; i < lottoSzamok.length; i++) {
            int randomSzam = randomGenerator(1, 90);
            nemMegfelelo = false;

            for (int j = 0; j < lottoSzamok.length; j++) {
                if (randomSzam == lottoSzamok[j]) {
                    nemMegfelelo = true;
                }
            }

            if (!nemMegfelelo) {
                lottoSzamok[i] = randomSzam;
            } else {
                i--;
            }
        }

        printArray(lottoSzamok);
    }

    public static void hatosLottoSzamok() {
        int[] lottoSzamok = new int[6];
        boolean nemMegfelelo;

        for (int i = 0; i < lottoSzamok.length; i++) {
            int randomSzam = randomGenerator(1, 45);
            nemMegfelelo = false;

            for (int j = 0; j < lottoSzamok.length; j++) {
                if (randomSzam == lottoSzamok[j]) {
                    nemMegfelelo = true;
                }
            }

            if (!nemMegfelelo) {
                lottoSzamok[i] = randomSzam;
            } else {
                i--;
            }
        }

        printArray(lottoSzamok);
        }
    }

