/*
 A program állítson elő véletlenszerűen két megegyező méretű mátrixot! Számítsa
 ki a és írja ki a két mátrix összegét  Minden részfeladatot külön metódus 
 valósítson meg!
 */
package hw_tomb_03;

public class Hw_tomb_03 {

    public static void main(String[] args) {
        int size1 = randomNumber(1, 9);
        int size2 = randomNumber(1, 9);

        int[][] multiArray1 = new int[size1][size2];

        int[][] multiArray2 = new int[size1][size2];
        
        int[][] sumArrays = new int[size1][size2];

        fillMultiArray(multiArray1);
        printMultiArray(multiArray1);

        System.out.println("------------");
        fillMultiArray(multiArray2);
        printMultiArray(multiArray2);

        System.out.println("--------------------");
        printMultiArray(sumMultiArray(multiArray1, multiArray2, sumArrays));

    }

    public static int randomNumber(int min, int max) {
        int size = (int) (Math.random() * (max - min) + min);
        return size;
    }

    public static void printMultiArray(int[][] multiArray) {
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(multiArray[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void fillMultiArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (Math.random() * (100 - 1) + 1);
            }
        }
    }

    public static int[][] sumMultiArray(int[][] array1, int[][] array2, int[][] sumArrays) {
      for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
            sumArrays[i][j] = array1[i][j] + array2[i][j];
        }
     }
        return sumArrays;
    }
}

