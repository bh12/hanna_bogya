
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BookingDAO {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/car_database?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String INSERT_NEW_BOOKING = "INSERT INTO booking (start_date, end_date) VALUES (?, ?)";

    public static void addBooking(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(INSERT_NEW_BOOKING)) {

            LocalDate start = LocalDate.parse(request.getParameter("start_date"));
            LocalDate end = LocalDate.parse(request.getParameter("end_date"));

            ps.setDate(1, Date.valueOf(start));
            ps.setDate(2, Date.valueOf(end));

            ps.executeUpdate();

            response.sendRedirect("MainMenuServlet");

            System.out.println("Sikeres");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
