package multithreadswritingme;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WriteMyNameThread extends Thread {

    static AtomicInteger counter = new AtomicInteger(1);

    static void incrementCounter(String name) {
        System.out.println(Thread.currentThread().getName() + ": " + counter.getAndIncrement() + name);
    }

    @Override
    public void run() {

        int rnd = (int) ((Math.random() * 300) + 5);
        while (counter.get() <= rnd) {
            incrementCounter(" Hanna");
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(WriteMyNameThread.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }
}
