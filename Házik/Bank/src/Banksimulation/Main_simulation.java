package Banksimulation;

import Banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main_simulation {

    public static void main(String[] args) {
        List<Transfer> trans = new ArrayList<>();

        File transferFile = new File("transfers.txt");
        try (FileReader fr = new FileReader(transferFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String source = transferArray[0];
                String target = transferArray[1];
                long ammount = Long.parseLong(transferArray[2]);

                Transfer t = new Transfer();
                trans.add(t);
                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben :(");
        }
        
        for (Transfer t : trans) {
            System.out.println(t);
        }
        
    }
    
    
        
}
