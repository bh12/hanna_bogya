<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>New Student</title>
    </head>
    <body>
    <c:if test="${gradeError != null}">
        <div class="alert alert-danger" role="alert">
            ${gradeError}
        </div>
    </c:if>
    <div class="container">
        <form method="POST" action="AddNewStudentServlet">
            <br>
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" placeholder="Enter name" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="course">Course:</label>
                <input type="text" class="form-control" placeholder="Enter course" id="course" name="course" required>
            </div> 
            <div class="form-group">
                <label for="first">First exam:</label>
                <input type="text" class="form-control" placeholder="First exam" id="first" name="first" required>
            </div> 
            <div class="form-group">
                <label for="second">Second exam:</label>
                <input type="text" class="form-control" placeholder="Second exam" id="second" name="second" required>
            </div> 
            <div class="form-group">
                <label for="final">Final exam:</label>
                <input type="text" class="form-control" placeholder="Final" id="final" name="final" required>
            </div> 
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</body>
</html>
