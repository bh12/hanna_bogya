/*
 Hozzunk létre egy 2D tömböt a következőnek megfelelően: az első dimenzióban 
 tároljuk el az alkalmazottak azonosítóját (egész számok, 1-től növekedve 1-gyel).
 A második dimenzióban tároljuk el az adott alkalmazotthoz tartozó havi fizetésüket.
 A havi fizetés a következőképp alakul:
 a.    Értékesítő kollégákról van szó, a fizetésük függ attól, hogy mennyi árut 
 adnak el. Az alap bérük 450.000 Ft, ehhez adjunk hozzá véletlenszerűen 
 50.000-125.000 Forintot.
 b.    Minden negyedév végén kapnak véletlenszerűen az adott havi fizetésükhöz 
 képest 5-15 százalékot.
 Írasd  ki a kollégák fizetését hónapra bontva.
 Adott a következő két String halmaz:
 a.    Laci, Józsi, Béla, Feri, Adri, Zsófi, Károly
 b.    Nagy, Veréb, Molnár, Kiss, Papp, Tóth
 Rendeljünk az előző feladatbeli azonosító számokhoz véletlenszerű neveket a 
 fentebb felsorolt keresztnevek és vezetékneveket felhasználva, és így Írasd  
 ki a kollégák fizetéseit.
 egész éves adat legyen eltárolva, tehát mind a 12 hónap (véletlenszerűen 
 generált) fizetése. Írjuk ki alkalmazottanként hogy mely hónapokra mekkora
 fizetést kaptak, és a végén az éves fizetésüket is írjuk ki.
 */
package employee_salary_stuff;

public class Employee_salary_stuff {

    public static void main(String[] args) {
        int[][] employeeData = new int[7][2];

        nameGenerator();

        System.out.println("Első negyed: január-március");
        printAQuarterYear(employeeData);
        System.out.println("Második negyed: április-június");
        printAQuarterYear(employeeData);
        System.out.println("Harmadik negyed: július-szeptember");
        printAQuarterYear(employeeData);
        System.out.println("Negyedik negyed: október-december");
        printAQuarterYear(employeeData);

    }

    public static void printSeparator() {
        System.out.println("----------------------------------");
    }

    public static void printEmployeeData(int[][] employeeData) {

        for (int i = 0; i < employeeData.length; i++) {
            for (int j = 0; j < employeeData[i].length - 1; j++) {

                System.out.print("Azonosító: " + employeeData[i][0] + "   " + "Fizetés: " + employeeData[i][1]);
                System.out.println("");
            }

        }
    }

    public static void salaryCalculatorPerMonth(int[][] employeeData) {

        for (int i = 0; i < employeeData.length; i++) {
            employeeData[i][0] = i + 1;  // sorszám
            employeeData[i][1] = 450_000 + randomNumberGenerator(50_000, 125_000);  // bonus
        }
    }

    public static void calculateBonusPercentage(int[][] employeeData) {
        for (int i = 0; i < employeeData.length; i++) {
            employeeData[i][0] = i + 1;  // sorszám
            employeeData[i][1] = (450_000 + randomNumberGenerator(50_000, 125_000))
                    + ((450_000 + randomNumberGenerator(50_000, 125_000)) * (randomNumberGenerator(5, 15) / 100));
        }
    }

    public static void printAQuarterYear(int[][] employeeData) {

        salaryCalculatorPerMonth(employeeData);
        printEmployeeData(employeeData);
        printSeparator();

        salaryCalculatorPerMonth(employeeData);
        printEmployeeData(employeeData);
        printSeparator();

        calculateBonusPercentage(employeeData);
        printEmployeeData(employeeData);
        printSeparator();

    }

    public static String[][] nameGenerator() {
        String firstNameofEmployees = "Laci,Józsi,Béla,Feri,Adri,Zsófi,Károly";
        String[] firstName = firstNameofEmployees.split(",");
        String lastNameofEmployees = "Nagy,Veréb,Molnár,Kiss,Papp,Tóth";
        String[] lastName = lastNameofEmployees.split(",");

        String[][] fullName = new String[7][3];

        for (int i = 0; i < fullName.length; i++) {
            for (int j = 0; j < fullName[i].length; j++) {
                fullName[i][0] = "Azonosító: " + (i + 1);
                fullName[i][1] = lastName[randomNumberGenerator(0, 5)];
                fullName[i][2] = firstName[i];

                System.out.print(fullName[i][j] + "  ");
            }
            System.out.println(" ");
        }
        return fullName;
    }

    public static int randomNumberGenerator(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);

    }
}
