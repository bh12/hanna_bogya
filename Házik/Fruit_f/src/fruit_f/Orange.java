package fruit_f;

public class Orange extends Fruits {
    
    private int price = 830;

    public Orange(double price, String colour) {
        super(price, "orange");
    }

    public String getColour(String colour) {
        return "orange";
    }
    
    public int  getPrice() {
    return (int) super.getWeight() * this.price;
    }

    @Override
    public String toString() {
        return "The colour of orange is always: " + getColour() + " and the price of " + super.getWeight() + "kg is: " + getPrice();
    }

}
