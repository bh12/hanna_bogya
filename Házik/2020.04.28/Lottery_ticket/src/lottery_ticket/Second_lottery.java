/*
 Olvassuk be az előző feladatban generált összes lottószelvényt, és írjuk ki 
 őket soronként a konzolra.
 */
package lottery_ticket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Second_lottery {

    public static void main(String[] args) {
        int counter = howManyFiles();
        readAndPrintTickets(counter);

    }

    public static void readAndPrintTickets(int counter) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < counter; i++) {
            try (BufferedReader buffR = new BufferedReader(new FileReader("lottery_ticket_" + i))) {
                String line = buffR.readLine();
                while (line != null) {
                    sb.append(line);
                    line = buffR.readLine();
                }
                sb.append("\n");

            } catch (IOException ioe) {
                System.out.println("nem talált");
            }
        }
        
        System.out.println(sb.toString());
    }

    public static int howManyFiles() {
        int counter = 1;
        boolean isIn = false;
        do {
            File file = new File("lottery_ticket_" + counter);
            if (file.isFile()) {
                counter++;
                isIn = true;
            } else {
                isIn = false;
            }
            
        } while (isIn);
        return counter;
    }

}
