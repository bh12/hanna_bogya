package exception;

public class WrongFormatException extends RuntimeException {

    public WrongFormatException() {
        this.getMessage();
    }
    
    @Override
    public String getMessage() {
        return "Please enter two numbers, dummy!";
    }
}
