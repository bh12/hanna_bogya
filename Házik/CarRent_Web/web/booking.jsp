<%-- 
    Document   : booking
    Created on : Jun 16, 2020, 8:32:53 PM
    Author     : Tamás
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1> Booking </h1>
        <br />
        <form action="BookingServlet" method="post">

            <label for="start_date"><b>Starts on</b></label>
            <input type="date" name="start_date" required>
            <br />
            <label for="end_date"><b>Ends on</b></label>
            <input type="date" name="end_date" required>
            <br />
            
            <input type="submit" value="Submit">
            </form>
            </body>
            </html>
