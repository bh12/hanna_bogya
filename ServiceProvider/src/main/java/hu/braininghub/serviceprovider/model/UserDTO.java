package hu.braininghub.serviceprovider.model;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    
    private String email;
    private String name;
    private String password;
    private Date timeOfRegister;
    private boolean isValidated;
    private String validationCode;
}
