/*
 Továbbá:
 1.    Parnacssorból kapott egész számoknak írja ki a konzolra az összegüket.
 2.    Parnacssorból kapott egész számok által alkotott intervallumból írjon ki 
 egy véletlen számot a konzolra.

 
 */
package hw_commlinearg_feladatok;

public class Hw_CommLineArg_feladatok {

    public static void main(String[] args) {
        
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);
        int number3 = Integer.parseInt(args[2]);

       System.out.println(number1 + number2 + number3);
       
       int randomNumber = (int) (Math.random() * (number3 + 1 - number1) + number1);
       
        System.out.println(randomNumber);
    }
}
