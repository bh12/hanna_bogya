
package Banksimulation.exeptions;

import Banksimulation.model.Transfer;

public class IllegalTargetClientException extends BankingException {

    public IllegalTargetClientException(Transfer transfer) {
        super("A cĂ©lszemĂ©ly nem egyezhet meg a kĂĽldĹ‘vel!", transfer);
    }

}
