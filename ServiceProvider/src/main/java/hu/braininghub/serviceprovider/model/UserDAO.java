package hu.braininghub.serviceprovider.model;

import hu.braininghub.serviceprovider.entity.UserEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.LoggerFactory;

@Singleton
public class UserDAO {

    @PersistenceContext
    EntityManager em;

    public void addUser(UserEntity user) {
        try {
            em.persist(user);
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error on creating user! REASON: {}", e.getMessage());
        }
    }

    public void removeUser(UserEntity user) {
        em.remove(user);
    }

    public UserEntity getUser(String email) {
        return em.find(UserEntity.class, email);
    }

    public void updateUserPassword(String email, String newPassword) {
        UserEntity user = getUser(email);
        user.setPassword(newPassword);
        em.merge(user);
    }

    public void validateUser(String email) {
        UserEntity user = getUser(email);
        user.setValidated(true);
        user.setValidationCode(null);
        em.merge(user);
    }

    public void cleanUpInvalidUsers() {
        final Long WEEKS_AFTER_INVALIDATED_USER_GETS_DELETED = Long.valueOf(3);

        LoggerFactory.getLogger(this.getClass().getName()).debug("User Database cleaning started at: {}", LocalDateTime.now());

        List<UserEntity> listOfInvalidUsers = em.createQuery("SELECT u FROM UserEntity u WHERE u.isValidated = false").getResultList();

        listOfInvalidUsers.stream()
                .filter(user -> user.getTimeOfRegister().before(Date.valueOf(LocalDate.now().minusWeeks(WEEKS_AFTER_INVALIDATED_USER_GETS_DELETED))))
                .forEach(user -> {
                    removeUser(user);
                });
    }
}
