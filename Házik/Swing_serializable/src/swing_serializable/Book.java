
package swing_serializable;

import java.io.Serializable;

public class Book implements Serializable{
    private String comp; 

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    @Override
    public String toString() {
        return "Book{" + "comp=" + comp + '}';
    }
    
    
}
