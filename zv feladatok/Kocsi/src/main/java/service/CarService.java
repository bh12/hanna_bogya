package service;

import entity.Car;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import mapper.CarMapper;
import model.CarDAO;
import model.CarDTO;

@Stateless
public class CarService {

    @Inject
    CarDAO dao;

    public CarDTO returnCarWithTheLessKm() {
        return CarMapper.CAR_MAPPER.toDTO(dao.returnCarWithTheLessKm());
    }

    public void addNewCar(String plateNumber, String type, Long km) {
        CarDTO dto = new CarDTO();
        dto.setPlateNumber(plateNumber);
        dto.setType(type);
        dto.setKm(km);

        dao.addNewCar(CarMapper.CAR_MAPPER.toEntity(dto));
    }
    
    public CarDTO findCarById(Long id){
        return CarMapper.CAR_MAPPER.toDTO(dao.findCarById(id));
    }

    public List<CarDTO> getCarDTOs(){
        return CarMapper.CAR_MAPPER.toDTOList(dao.getCars());
    }
}
