<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <%@include file="cssForHeadLine.jsp" %>
        <%@include file="head.jsp" %>
        <title>Change Password</title>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>
        <c:if test="${newPwdError != null}">
            <div class="alert alert-danger" role="alert">
                ${newPwdError}
            </div>
        </c:if>
        <div class="container">
            <h1>Please enter new password:</h1>
            <form method="POST" action="PasswordRecoveryServlet" class="was-validated">
                <div class="form-group col-6">
                    <label for="newPassword">New password:</label>
                    <input type="password" class="form-control" id="newPassword" name="newPassword" required>
                </div>
                <div class="form-group col-6">
                    <label for="confirm_newPassword">Confirm new password:</label>
                    <input type="password" class="form-control" id="confirm_newPassword" name="confirm_newPassword" required>
                </div>
                <button type="submit" class="btn btn-outline-dark">Change it</button>
            </form>
        </div>
        <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>
    </body>
</html>
