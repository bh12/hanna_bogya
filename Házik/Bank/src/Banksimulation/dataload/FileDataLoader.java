package Banksimulation.dataload;

import Banksimulation.BankApplication;
import Banksimulation.model.Client;
import Banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class FileDataLoader implements DataLoader {

    @Override
    public void loadInitialData() {
        clientDataLoader();
        readTransfer();
    }

    public void clientDataLoader() throws NumberFormatException {
        File clientFile = new File("clients.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                BankApplication.clients.add(c);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben :(");
        }
    }

    public void readTransfer() {

        File transferFile = new File("transfers.txt");

        try (FileReader fr = new FileReader(transferFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String sourceId = transferArray[0];
                String sourceAccountNumber = transferArray[1];
                long sourceBalance = Long.parseLong(transferArray[2]);
                String targetId = transferArray[3];
                String targetAccountNumber = transferArray[4];
                long targetBalance = Long.parseLong(transferArray[5]);
                long ammount = Long.parseLong(transferArray[6]);

                Client sourceClient = new Client(sourceId, sourceAccountNumber, sourceBalance);
                Client targetClient = new Client(targetId, targetAccountNumber, targetBalance);
                Date now = new Date();

                Transfer t = new Transfer();
                t.setSource(sourceClient);
                t.setTarget(targetClient);
                t.setAmmount(ammount);
                t.setDateOfCompletion(now);

                BankApplication.transfers.add(t);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben :(");
        }
    }
    
  

}
