<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">           
            <form method="POST" action="PalindromServlet">
                <div class="form-group">
                    <label for="word">Give me a word:</label>
                    <input type="text" class="form-control" placeholder="Check for palindrome" name="word" id="word">
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <br>
        <div class="form-group">
            <label for="isPalindrome">Is palindrome: ${isPalindrome}</label>
        </div>
    </body>
</html>
