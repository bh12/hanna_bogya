/*
 Kérjünk be a felhasználótól szöveges sorokat addig, amíg egy üres sort nem ír be.
 A sorokat sorszámmal írjuk ki egy fájlba „scanner_text.txt” néven.
 */
package file_gyakorlatok;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Third {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Add be a sorokat: ");
        
        File file = new File("scanner_text");
        int lineCount = 1;
        String line = sc.nextLine();
        

        try (FileWriter writer = new FileWriter(file, true)) {

            while (!line.equals("")) {
               
                writer.write(lineCount + ": " + line + "\n");
                line = sc.nextLine();
                lineCount++;
            }
        } catch (IOException ioe) {
            System.out.println("Nem sikerült");
        }

    }
}
